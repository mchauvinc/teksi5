
/**
 * Module dependencies.
 */

var express = require('express')
  , mongoose = require('mongoose')
  , routes = require('./routes')
  , driver = require('./routes/driver')
  , Driver = require('./data/models/Driver')
  , Client = require('./data/models/Client')
  , http = require('http')
  , path = require('path')
  , socket = require('./routes/socket')
  , maps = require('googlemaps')
  , util = require('util')
  , _ = require('lodash')
  , config = require('./routes/config');



var app = express();
var options = {
  server: {
    socketOptions: { keepAlive: 1},
    auto_reconnect: true
  }
};

var isConnectedToDb = false;
exports.isConnectedToDb;

var db = mongoose.connection;


//Some events for mongoose connection
  db.on('connecting', function() {
    console.log('connecting to MONGODB...');
  });

  db.on('error', function(error) {
    console.error('Error in MONGODB connection: ' + error);
    mongoose.disconnect();
    isConnectedToDb = false;
  });
  db.on('connected', function() {
    isConnectedToDb = true;
    console.log('MONGODB connected!');
  });
  db.once('open', function() {
    console.log('MONGODB connection opened!');
  });
  db.on('reconnected', function () {
    console.log('MONGODB reconnected!');
    isConnectedToDb = true;
  });

  //If we are disconnected from database, we retry to connect after 30 seconds
  db.on('disconnected', function() {
    isConnectedToDb = false;
    setTimeout(function(){
      console.log('MONGODB disconnected!');
      mongoose.connect(config.DB_CONNECTION, options);
    },30000);
    
  });


//The first connection
mongoose.connect(config.DB_CONNECTION, options);


// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/driver', driver.driver);
var server = http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

socket.socket().startSocket(server);