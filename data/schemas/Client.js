var mongoose = require('mongoose');
var ClientSchema = new mongoose.Schema({

	id: mongoose.Schema.Types.ObjectId,
	Session: String,
	Username: String,
	// Language: String
	
}, { collection: 'Client' }, { versionKey: false });

module.exports = ClientSchema;