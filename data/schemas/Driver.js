var mongoose = require('mongoose');
var DriverSchema = new mongoose.Schema({

	_id: mongoose.Schema.Types.ObjectId,
	StrId: String,
	Session: String,
	Username: String,
	CarModel: String,
	Seats: String,
	Plate: String,
	Fname: String,
	Lname: String,
	PhoneNumber: String,
	Email: String,
	img: String, 
	RegistrationId: String,
	Type: Number,
	Language: String

}, { collection: 'Driver' }, { versionKey: false });

module.exports = DriverSchema;