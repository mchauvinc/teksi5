var mongoose = require('mongoose');
var geolib = require('geolib');
var positionService = require('../../routes/positions/position');
var config = require('../../routes/config');

var AnswerSchema = new mongoose.Schema({
	Driver: {_id: mongoose.Schema.Types.ObjectId},
	Response: Number
}, { _id: false });

var TripsSchema = new mongoose.Schema({

	id: mongoose.Schema.Types.ObjectId,

	Driver:
	{
		_id: mongoose.Schema.Types.ObjectId,
		StrId: String,
		Session: String,
		Username: String,
		CarModel: String,
		Seats: String,
		Plate: String,
		Fname: String,
		Lname: String,
		PhoneNumber: String,
		Email: String,
		img: String, 
		RegistrationId: String,
		Type: Number,
		Language: String

	},
	Client: 
	{
		_id: mongoose.Schema.Types.ObjectId,
		Session: String,
		Username: String,
		Fname: String,
		Lname: String,
		PhoneNumber: String,
		Email: String,
		RegistrationId: String, 
		Type: Number
	},

	
	DriverLanguageAddress:
	{
		PickupAddress: String,
		DropOffAddress: String
	},
	

	Address:
	{
		Pickup:{
			Description: String,
			Latitude: Number,
			Longitude: Number
		},

		Destination:{
			Description: String,
			Latitude: Number,
			Longitude: Number
		}
	},

	Passengers: {type: Number, default: 1},
	CreationTime: {type: Date, default: Date.now},
	RequestTime: Date,

	DropoffTime: Date,
	PickUpTime: Date,

	InAdvance: {type: Boolean, default: false},
	DriverNotes: String,

	EstimatedArrivalTime: Date,
	Tips: {type: Number, default: 0},
	Distance: Number,
	Duration: Number,
	Status: {type: Number, default: config.TRIP_STATUS_CREATED},

	DriversProposed:	{proposed_drivers: [AnswerSchema]}

}, { collection: 'Trips' },  { versionKey: false });

TripsSchema.methods.isValid = function()
{
	return (this.Address.Pickup != 'undefined' && this.Address.Destination != 'undefined' && !! this.RequestTime
			&& this.Address.Pickup.Latitude != null 
			&& this.Address.Destination.Latitude != null
			&& this.Address.Pickup.Description != 'undefined'
			&& this.Address.Destination.Description != 'undefined'
			&& this.Address.Pickup.Description != null
			&& this.Address.Destination.Description != null);
}

TripsSchema.methods.distance = function() //In meters
{
	var pickupPosition = positionService.Position(this.Address.Pickup.Latitude, this.Address.Pickup.Longitude);
	var destinationPosition = positionService.Position(this.Address.Destination.Latitude, this.Address.Destination.Longitude);
	return geolib.getDistance(pickupPosition.toDict(), destinationPosition.toDict());
}

TripsSchema.methods.price = function()
{
	var distance = this.distance();
	return config.price(distance);
}

TripsSchema.methods.pickupLatLng = function()
{	
	return config.latlng(this.Address.Pickup.Latitude, this.Address.Pickup.Longitude);
}

TripsSchema.methods.destinationLatLng = function()
{	
	return config.latlng(this.Address.Destination.Latitude, this.Address.Destination.Longitude);
}

module.exports = TripsSchema;