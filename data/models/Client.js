var mongoose = require('mongoose');
var ClientSchema = require('../schemas/Client');
var Client = mongoose.model('Client', ClientSchema);
module.exports = Client;