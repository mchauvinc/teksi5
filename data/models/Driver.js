var mongoose = require('mongoose');
var DriverSchema = require('../schemas/Driver');
var Driver = mongoose.model('Driver', DriverSchema);
module.exports = Driver;