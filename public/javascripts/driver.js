
	
var socket = io.connect('/'),
// var socket = io.connect('http://localhost:3000'),
logger = document.getElementById( 'logger' ),
message = document.getElementById( 'message' );	

socket.on('connect',function(a){
	logger.innerText = "Connected";
	socket.emit("taxi:register");
	setInterval( function(){
		socket.emit("taxi:position:update", {lat: Math.random()*100,lng:Math.random()*100});
	}, 1000 );
});