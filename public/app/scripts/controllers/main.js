'use strict';

angular.module('publicApp')
  .controller('MainCtrl', [ '$scope', 'socket', function ($scope, socket) {
    $scope.connected = false;
	socket.on('connect', function(){
		$scope.connected = true;
	});
	/*socket.on('disconnect', function(){
		$scope.connected = false;
	});*/
	$scope.connect = function() {
		
	}
	$scope.disconnect = function() {
		io.sockets[window.location.origin].disconnect();
	}
	
	socket.on( 'taxis:refresh', function(taxis) {
		$scope.taxis = taxis;
	});
  }]);
