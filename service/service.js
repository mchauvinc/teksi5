﻿//Run node.js service.js to install this node server as a windows service

var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'Node Backend Server',
  description: 'Backend Node server for Taxihero',
  script: 'C:\\nodeserver\\server.js'
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
});

svc.install();