var io = require('socket.io-client');

var DRIVER_TOKEN = 'hello';

var SERVER_URL = 'http://localhost:3000';

var request = require('superagent');
var expect = require('expect.js');

function createNewSocket(){
	return io.connect(SERVER_URL, {
		'force new connection': true
	});
}

function removeAllListeners(socket){
	socket.socket.$events = {};
}

describe('Taxi registration', function(){
	it('Should receive a taxi registered event', function(done){
		var socket = io.connect(SERVER_URL);
		socket.on('taxi:registered', function(){
			socket.removeAllListeners('taxi:registered');
			done();
		});
		socket.emit('taxi:register',DRIVER_TOKEN);
	});

});

describe('Taxi position', function(){
	var socketMe,
		socketOther;
	beforeEach(function(){
		socketMe = createNewSocket();
		socketOther = createNewSocket();
	});
	afterEach(function(){
		removeAllListeners(socketMe);
		removeAllListeners(socketOther);
	});
	it('Should update the correct position', function(done){
		var l = 3.1,
			ln = 100;
		socketMe.on('taxi:registered', function(){
			socketMe.on('test:getalltaxipositions', function(data){
				expect(parseFloat(data[DRIVER_TOKEN].lat)).to.equal(l);
				expect(parseFloat(data[DRIVER_TOKEN].lng)).to.equal(ln);
				done();
			});
			socketMe.emit('taxi:position:update',DRIVER_TOKEN,{
				lat:l,
				lng: ln
			});
			socketMe.emit('test:getalltaxipositions');
		});
		socketMe.emit('taxi:register',DRIVER_TOKEN);
	});
	it('Should NOT update other drivers', function(done){
		var l = 3.1,
			ln = 100,
			otherDriverToken = DRIVER_TOKEN+'2';
		socketMe.on('taxi:registered', function(){
			socketOther.on('taxi:registered', function(){
				socketMe.on('test:getalltaxipositions', function(data){
					expect(parseFloat(data[otherDriverToken].lat)).to.equal(0);
					expect(parseFloat(data[otherDriverToken].lng)).to.equal(0);
					done();
				});
				socketMe.emit('test:getalltaxipositions');						
			});
			socketOther.emit('taxi:register',otherDriverToken);

		});
		socketMe.emit('taxi:register',DRIVER_TOKEN);
		
	});

});

