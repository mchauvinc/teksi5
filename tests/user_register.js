var io = require('socket.io-client');

var USER_TOKEN = '12345';

var SERVER_URL = 'http://localhost:3000';

var request = require('superagent');
var expect = require('expect.js');

function createNewSocket(){
	return io.connect(SERVER_URL, {
		'force new connection': true
	});
}

function removeAllListeners(socket){
	socket.socket.$events = {};
}

describe('User registration', function(){
	it('Should receive a user registered event', function(done){
		var socket = io.connect(SERVER_URL);
		socket.on('user:registered', function(){
			socket.removeAllListeners('user:registered');
			done();
		});
		socket.emit('user:register',USER_TOKEN);
	});

});



