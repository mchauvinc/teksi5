var io = require('socket.io-client');

var DRIVER_TOKEN = 'hello';
var USER_TOKEN = '12345';

var SERVER_URL = 'http://localhost:3000';
var MONGODB_URL = 'mongodb://localhost/teksi';

var request = require('superagent');
var expect = require('expect.js');
var Trip = require('../data/models/Trips');
 mongoose = require('mongoose');

// 	mongoose.connect(MONGODB_URL);

function createNewSocket(){
	return io.connect(SERVER_URL, {
		'force new connection': true
	});
}

function removeAllListeners(socket){
	socket.socket.$events = {};
}

describe('Trip creation', function(){

	var validTrip = {
		RequestTime:new Date(),
		Pickup:{
			description: 'Jalan Alor',
			Latitude:3.1,
			Longitude:101
		},
		Destination:{
			description: '3445321',
			Latitude:3.2,
			Longitude:102
		}
	};



	it('Should inform the user that a trip has been created', function(done){
		var socket = io.connect(SERVER_URL);
		socket.on('user:registered', function(){


			socket.on('trip:create:success', function(){
				done();
			});
			socket.emit('trip:create',USER_TOKEN,validTrip);

		});
		socket.emit('user:register',USER_TOKEN);

		

	});

	var testDescription = {
			description: '3445321',
			Latitude:3.2,
			Longitude:102
		};

		

	it('Should save the trip into database', function(done){
		/*beforeEach(function(){
			Trip.findOne({ Destination:testDescription }).remove();
		});*/
		Trip.findOne({Destination:testDescription }, function(err, res){
			expect(res.Destination.description).to.equal(validTrip.Destination.description);
			//expect(1).to.equal(1);
			done();
		});

	});

	
});

