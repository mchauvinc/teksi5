var io = require('socket.io-client');

var DRIVER_TOKEN = 'hello';


var USER_TOKEN = '12345';

var SERVER_URL = 'http://localhost:3000';

var request = require('superagent');
var expect = require('expect.js');

function createNewSocket(){
	return io.connect(SERVER_URL, {
		'force new connection': true
	});
}

function removeAllListeners(socket){
	socket.socket.$events = {};
}

describe('Taxis registration', function(){
	 taxiSocket = [];

	
		for(var i = 0; i < 5; i++)
		{
			console.log('Creating socket ', i);
			taxiSocket[i] = createNewSocket();
		
			taxiSocket[i].on('taxi:registered', function(){
				done();
			});
			taxiSocket[i].emit('taxi:register', DRIVER_TOKEN + i);
		}

		var validTrip = {
		RequestTime:new Date(),
		Pickup:{
			description: 'Jalan Alor',
			Latitude:3.1,
			Longitude:101
		},
		Destination:{
			description: '3445321',
			Latitude:3.2,
			Longitude:102
		}

	};

	afterEach(function(){
		for(var i = 0; i < 5; i++)
		{
			console.log('Removing socket ', i);
			removeAllListeners(taxiSocket[i]);
		}
		
	});
	
	//taxiSocket[0].emit('taxi:register',DRIVER_TOKEN1);
});
