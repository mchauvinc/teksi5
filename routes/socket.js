// npm modules
var io = require('socket.io');
var _ = require('lodash');
var gm = require('googlemaps');

// services & stuff
var config = require('./config');
var taxisService = require('./taxis/taxis');
var usersService = require('./users/users');
var tokensService = require('./tokens/tokens');
var events = require('./event');
var tripService = require('./trip/trip');
var positionService = require('./positions/position');

var tripsManager = require('./tripservice/trips');


// mongodb schemas & models
var Trip = require('../data/models/Trips');
var Client = require('../data/models/Client');
var Driver = require('../data/models/Driver');

var taxiKiller = setInterval(function() { taxisService.taxiKiller() }, config.TAXI_KILLER_TIMEOUT * 1000 * 60);

var debouncedPositionUpdate = _.debounce(taxiPositionUpdate, 1, true);

exports.socket = function(){
	return {
		startSocket: function(server){
			setUpListeners(io.listen(server, {
				origins: '*:*', 
				log: true,
			}));
		}
	}
}

function setUpListeners(io) {
	// Create event for connected taxi
	io.on("connection", function(socket){
		var clients = io.sockets.clients();
		setUpTaxiEvents(socket);
		setUpUserEvents(socket);
		setUpDevEvents(socket);
	});
}

function taxiRegister(socket, token, language)
{	
	var index = _.indexOf(config.SUPPORTED_LANGUAGES, language);

	if(token && token.length < 100)
	{	
		// Removing unecessary quotes from token
		var token = token.replace(/\"/g, "");
		// Getting all tokens
		var allTokens = tokensService.getAllTokens();
		if(allTokens)
		{	
			// Getting an id corresponding to the token
			var id = tokensService.get(token);
			var taxi;
			//if no id is found
			if(!id)
			{	//We look into mongo db given the Session
				Driver.findOne({Session:token}, function(err, letaxi){
					//If nothing is found, the taxi is unknown
					if(!letaxi)
					{	
						socket.emit('taxi:register:error', "TAXI_NOT_FOUND");
					}
					//Else, we can find its _id
					else
					{	//Looking for token keys who match this _id
						var letoken =  _.findKey(allTokens, function(x) {
	  						return x == letaxi._id.toString();
						});
						//If token found
						if(letoken)
						{	//We update the _id that corresponds to the token key
							tokensService.set(token, letaxi._id);
							//Then we remove the useless token
							tokensService.unset(letoken);
							// And we can update the socket
							taxi = taxisService.get(letaxi._id);
							letaxi.token = token;
							if(taxi)
							{
								taxi.socket = socket;
								if(index != -1)
								{
									taxi.language = config.formatLanguage(language);
								}
							}
							socket.emit("taxi:registered", letaxi.Language);
						}
						//If no token matches
						else
						{	//The taxi isn't already in memory so we need to create it
							// Registering token, taxi
						var atoken = tokensService.create(token, letaxi._id);
							tokensService.register(atoken);
							taxi = taxisService.create(letaxi._id, socket);
							taxi.socket = socket;
							taxisService.register(taxi);
							taxi.driverObject = letaxi;
							taxi.token = token;
								
								if(index != -1)
								{
									taxi.language = config.formatLanguage(language);
								}
								else
								{
									taxi.language = config.LANGUAGE_CN;
								}

							delete taxi.driverObject.Password;
							console.log('Creating new taxi instance');
							console.log('taxi registered with token ', token);
							socket.emit("taxi:registered", letaxi._id);
						}
					}
				});
			}
			//We found an _id so its corresponding taxi is already in memory
			else
			{
				taxi = taxisService.get(id);
				if(taxi)
				{
					// Updating the taxi socket
					taxi.socket = socket;
					taxi.token = token;
					if(index != -1)
					{
						taxi.language = language;
					}
					console.log('taxi registered with token ', token);
					socket.emit("taxi:registered", id);
					
						// If taxi has a currentTrip, we need to send him again the trip details
						if(taxi.currentTrip != null)
						{
							Trip.findOne({'_id': taxi.currentTrip},function(err, letrip){
								if(letrip)
								{
									console.log('Trip backup was sent to driver ', id);
									socket.emit("trip:exist",  letrip);
								}
							});
						}
				}
				else
				{
					console.log('ERROR IN TAXI REGISTER: TAXI NOT FOUND');
				}			
			}
		}
	}
	else
	{
		console.log('ERROR In taxiRegister: Token is null');
	}
}

function taxiPositionUpdate(socket, token, data)
{	
	//var d = new Date()
	//console.log(d, ': GOT position update from taxi with token: ', token);


	// usual stuff: need to get id from token, then taxi from id
		if(token && token.length < 100 && data.lat && data.lng)
		{
			var token = token.replace(/\"/g, "");
			var id = tokensService.get(token);
			if(id)
			{
				var taxi = taxisService.get(id);
				if(taxi) 
				{
					// Updating taxi position
					taxi.lat(data.lat);
					taxi.lng(data.lng);

					// If taxi has a current trip and is not arrived to the pickupPosition
					if(taxi.currentTrip && taxi.arrivedAtPickup == false)
					{
							// Find the trip by id
							Trip.findOne({'_id': taxi.currentTrip},function(err, letrip){
								if(letrip)
								{
									// Then send its new position to the trip client
									var position = {
										_id: id,
										TripId: letrip._id,
										Position: data,
										RemainingTime: taxi.getArrivalCountdown(letrip.Address),
										ArrivalTime: taxi.getArrivalTime(letrip.Address)
									};
									var user = usersService.get(letrip.Client._id);
									if(user && user.socket)
									{
										console.log('Sent position update to client: ', letrip.Client._id, ' for trip: ', position.TripId  )
										user.socket.emit('yourdriver:position', position);	 
									}
								}
							});
					
						
					}
					// Position update confirmation
					// console.log('Updated position of taxi: ', id, ' lat: ', data.lat, ' lng: ', data.lng);
					socket.emit("taxi:position:updated");
				}
			}
			else
			{	
				// If token is invalid or has changed
				//console.log('ERROR In updatePosition: Taxi with token:', token, ' not registered');
				//taxiRegister(socket, token);
			}
		}
		else
		{
			console.log('ERROR in updatePosition: token or position not valid');
		}
}

function setUpTaxiEvents(socket){ //Driver socket

	socket.on('test:getalltaxipositions', function(){
	var taxis = taxisService.getAllTaxis(),
			simpleTaxis = {};

		_.each(taxis, function(x, k){
			simpleTaxis[x.token] = {
				token: x.token,
				lat: x.position.lat(),
				lng: x.position.lng()
			};
		});
		socket.emit('test:getalltaxipositions',simpleTaxis);
 	});
 


	socket.on("taxi:register", function(token, language){
			console.log('GOT taxi:register with token: ', token);
			taxiRegister(socket, token, language);
	});	

	socket.on("taxi:unregister",function(token){
		console.log('GOT taxi:unregister with token: ', token);
		if(token)
		{
			var token = token.replace(/\"/g, "");
		}
		var id = tokensService.get(token);
		if(id)
		{
			var taxi = taxisService.get(id);
			if(taxi) 
			{
				if(taxi.tripCount == 0 && taxi.currentTrip == null)
				{
					taxisService.unregister(id);
					tokensService.unset(token);	
					socket.emit("taxi:unregistered",  id);
				}
				else
				{
					console.log('ERROR In taxiUnRegister: taxi ',id ,' is enrolled in a trip and cannot be unregistered');
					socket.emit("taxi:unregister:error", id);
				}				
			}
		}	
	});

	socket.on("taxi:position:update", function(token, data){ //data : new position {lat:'100.3', lng:'7.122342'}

	    // taxiPositionUpdate(socket, token, data);
	    debouncedPositionUpdate(socket, token, data);
	});


socket.on("taxi:trip:taxi_arrived_at_destination", function(token, tripId){
		if(token)
		{
			var token = token.replace(/\"/g, "");
		}
		var id = tokensService.get(token);
		if(id)
		{
			var taxi = taxisService.get(id);
			// Resetting taxi's current trip
			taxi.currentTrip = null;
			taxi.arrivedAtPickup = false;
			taxi.lastTripCompleted = new Date();
			taxi.isLocked = false;
			Trip.findOne({'_id': tripId},function(err, letrip){
					if(letrip)
					{	
						// Updating trip informations
						letrip.DropoffTime = Date.now();
						letrip.Status = config.TRIP_STATUS_DONE;
						letrip.save(function(err){
							if(!err)
							{	
								console.log('TRIP UPDATED');
							}
						});

						if(taxi && letrip.Client._id)
						{
							user = usersService.get(letrip.Client._id);
							if(user && user.socket)
							{
								user.socket.emit("trip:completed", tripId);

								user.tripId = null;

								console.log('Trip completed, user tripId: ', user.tripId);
							}
						}
					}
				});

				tripsManager.unregister(tripId);
				delete taxi.trips[_.indexOf(taxi.trips, tripId)];
				taxi.tripCount --;
				
		}
		
		
	});

socket.on("taxi:trip:taxi_arrived_at_pickup", function(token, tripId){ // + TRIP ID
	console.log('In taxi arrived at pickup: ', token, tripId);
	if(token)
	{
		var token = token.replace(/\"/g, "");
	}
	var id = tokensService.get(token);
	if(id)
	{
		var taxi = taxisService.get(id);
		taxi.currentTrip = tripId;
		taxi.arrivedAtPickup = true;
		// if(taxi && taxi.clientSocket)
		Trip.findOne({'_id': tripId}, function(err, letrip){
			if(letrip)
			{	
				letrip.PickUpTime = Date.now();
				letrip.save(function(err){
					if(!err)
					{
						console.log('trip Saved in PickUpTime', letrip.PickUpTime);
					}
				});
				console.log('In taxi arrived at pickup: ', letrip.Client._id);
				if(taxi && letrip.Client._id)
				{
					var user = usersService.get(letrip.Client._id);
					if(user && user.socket)
					{
						user.socket.emit("trip:taxi:arrived", tripId);
					}
				}
			}
		});	
	}
});

	socket.on("taxi:cancelled", function(token, tripId){
		if(token)
		{
			var token = token.replace(/\"/g, "");
		}
		var id = tokensService.get(token);
		if(id)
		{
			var taxi = taxisService.get(id);
			if(taxi)
			{
				// taxi.currentTrip = tripId;
			// if(taxi && taxi.clientSocket)
			Trip.findOne({'_id': tripId},function(err, letrip){
					if(letrip)
					{	
						console.log('TRIP FOUND IN CANCEL');
						letrip.Status = config.TRIP_STATUS_CANCELLED;
						letrip.save(function(err){
							if(!err)
							{	
								console.log('TRIP SAVED IN TAXI CANCEL');
							}
						});
						var user = usersService.get(letrip.Client._id);
						if(user && user.socket)
						{
							console.log('GOT USER AND SOCKET');
							
							if(letrip.InAdvance == true)
							{
								user.socket.emit("trip:advance:taxi:cancelled", tripId);
							}
							else
							{
								user.socket.emit("trip:taxi:cancelled", tripId);
							}
							user.tripId = null; 
						}
						else
						{
							console.log('In taxi cancel, did not got user');
						}

					}
				});

			delete taxi.trips[_.indexOf(taxi.trips, tripId)];
			tripsManager.unregister(tripId);
				if(taxi.currentTrip && taxi.currentTrip == tripId)
				{
					taxi.currentTrip = null;
				}
				taxi.tripCount --;
				
				if(taxi.tripCount <= 0)
				{
					taxi.isLocked = false;
				}
				
			}
		}
	});

	socket.on("taxi:started:journey", function(token, tripId){
		console.log('IN TAXI STARTED JOURNEY');
		if(token)
		{	
			var token = token.replace(/\"/g, "");
		}
		var id = tokensService.get(token);
		if(id)
		{
			var taxi = taxisService.get(id);
			if(taxi)
			{
				console.log('GOT TAXI');
				// Setting taxi's current trip
				taxi.currentTrip = tripId;
				Trip.findOne({'_id': tripId},function(err, letrip){
					if(letrip)
					{	
						console.log('GOT TRIP');
						var user = usersService.get(letrip.Client._id);
						if(user && user.socket)
						{	
							console.log('GOT USER');
							if(letrip.InAdvance == true)
							{
								user.socket.emit("trip:taxi:started:journey", letrip);
							}
							
							user.tripId = tripId; 
							console.log('SET TRIP ID OF USER IN TAXI STARTED JOURNEY');
						}

					}
				});
			}
		}
	});

	socket.on('trip:driver:answer', function(token, tripId, answer){ //answer: 0 = no, 1 = yes
		if(token)
		{
			var token = token.replace(/\"/g, "");
			var id = tokensService.get(token);
			if(id)
			{
				var taxi = taxisService.get(id);
				if(taxi) 
				{
					taxi.socket.emit('trip:got:answer', tripId);
				}
				// Looking for the trip
				Trip.findOne({ '_id': tripId }, function (err, res) {
					if(!err)
					{
											// If the trip was just created
					if(res.Status == config.TRIP_STATUS_CREATED)
					{
						// Getting the list of drivers who proposed
						var driversList = res.DriversProposed.proposed_drivers;
						// Trying to find the current driver in this list
						var index = _.findIndex(driversList, function(x){
							return id.toString() == x.Driver._id.toString();
						});
						// If no one is found
						if(index == -1)
						{	
							// Then we need to add his answer to the list
							console.log('Driver never answered to this trip before');
								var response = {
									Driver: {_id: id},
									Response: answer
								};
								res.DriversProposed.proposed_drivers.push(response);
						}
						else
						{
							// Else we just update his answer
							console.log('Driver already answered to this trip');
							res.DriversProposed.proposed_drivers[index].Response = answer;
						}
						// Then we save it into database
						res.save(function(err, res){
							//console.log('In trip answer, updated trip', err, res);
							// And send an event to trip/handleDriversAnswers
							if(answer == 0)
							{
								taxi.isLockedForDispatch = false;
							}
							events.emitter.emit('ANSWER_FROM_DRIVER', tripId, res.DriversProposed.proposed_drivers.length);
						});
					}
					else
					{
						console.log('ERROR in DRIVER ANSWER: Cannot give an answer for a finished or cancelled trip.')
					}
					}
					else
					{
						console.log('Error in mongodb');
					}

				});
			}
		}	
		console.log('GOT answer: ', answer, ' for tripId: ', tripId, ' with token: ', token);
	});
	
}

function userRegister(socket, token){
		if(token)
		{
			var token = token.replace(/\"/g, "");
		}
		
				console.log('In User register:', token);

		var allTokens = tokensService.getAllTokens();
	
		var id = tokensService.get(token);
		var user;
		//if no _id is found
		if(!id)
		{	//We look into mongo db given the Session
			Client.findOne({Session:token},function(err, leclient){
				//If nothing is found, the user is unknown
				if(!leclient)
				{	
					socket.emit('user:register:error','USER_NOT_FOUND');
				}
				//Else, we can find its _id
				else
				{	//Looking for token keys who match this _id
					var letoken =  _.findKey(allTokens, function(x) {
  						return x == leclient._id.toString();
					});
					//If 1 token found
					if(letoken)
					{	
						//We update the _id that corresponds to the token key
						tokensService.set(token, leclient._id);
						//Then we remove the useless token
						tokensService.unset(letoken);
						user = usersService.get(leclient._id);
						if(user)
						{
							user.socket = socket;
						}
						socket.emit("user:registered", leclient._id);
					}
					//If no token matches
					else
					{	//The user isn't already in memory
					var atoken = tokensService.create(token, leclient._id);
						tokensService.register(atoken);
						user = usersService.create(leclient._id, socket);
						usersService.register(user);
						user.clientObject = leclient;
						user.socket = socket;
						delete user.clientObject.Password;
						console.log('creating new client instance');
						console.log('user registered with token ', token);
						socket.emit("user:registered", leclient._id);
					}
				}
			});
		}
		//We found an _id so its corresponding user is already in memory
		else
		{
			user = usersService.get(id);
			if(user)
			{
				user.socket = socket;
				console.log('user registered with token ', token, ' locked: ', user.isLocked, ' tripId: ', user.tripId);
				socket.emit("user:registered",  id);

					// We need to send him again details about his trip
					if(user.tripId != null)
						{
							console.log('User trip id is not null, looking for trip');

							Trip.findOne({'_id': user.tripId},function(err, letrip){
								if(letrip)
								{	
									console.log('Trip found');
									socket.emit("trip:exist",  letrip);
								}
							});
						}
				//}
			}
			else
			{
				console.log("Error USER_NOT_FOUND");
			}
			

		}
	}


function setUpUserEvents(socket) { //Client Socket
	socket.on("user:register", function(token){
		userRegister(socket, token);
	});

	socket.on("user:unregister",function(token){
		if(token)
		{
			var token = token.replace(/\"/g, "");
		}
			var id = tokensService.get(token);
			if(id)
			{
				var client = usersService.get(id);
				if(client) 
				{
					if(client.tripId == null)
					{
						usersService.unregister(id);
						tokensService.unset(token);	
						socket.emit("user:unregistered", id);
					}
					else
					{
						console.log('ERROR In userUnRegister: user ',id ,' is enrolled in a trip and cannot be unregistered');
						socket.emit("user:unregister:error",  id);
					}
				}
			}
			
		});

	socket.on("trip:create", function(token, trip){
		console.log('GOT trip:create');
		console.log('TRIP OBJECT:', trip);
		if(token)
		{
			var token = token.replace(/\"/g, "");
		}
		// Getting id from token
		var id = tokensService.get(token);
		if(id)
		{	// Getting client from token
			console.log('GOT id');
			var client = usersService.get(id);
			if(client)
			{	
				client.lastTripCreated = new Date();
				console.log('GOT client');
				// Creating trip
				var letrip = new Trip();
				letrip.Client = client.clientObject;
				if(trip.Address)
				{
					letrip.Address.Pickup = trip.Address.Pickup;
					letrip.Address.Destination = trip.Address.Destination;
				}
				else
				{
					letrip.Address.Pickup = trip.Pickup;
					letrip.Address.Destination = trip.Destination;
				}
				letrip.RequestTime = trip.RequestTime;
				letrip.Tips = trip.Tips;
				letrip.Status = config.TRIP_STATUS_CREATED;
				if(!trip.inAdvance)
				{
					letrip.InAdvance = false;
				}
				else
				{
					letrip.InAdvance = trip.inAdvance;
				}
				
				letrip.DriverNotes = trip.Note;


				if(letrip.isValid())
				{
					console.log('Trip is valid');
					//client.isLocked = true;
					var userId = id;
					letrip.save(tripService.onTripSave.bind(null, socket, userId, false)); //Trip saved
				}
				else
				{
					socket.emit('trip:create:error', "INVALID_TRIP_DATA");
				}
			}
			else
			{
				console.log('ERROR IN TRIP CREATE : id found, but it doesnt belong to a user');
			}
		}
		else
		{
			socket.emit('trip:create:error', "USER_NOT_LOGGED_IN");
			userRegister(socket, token);
		}

	});

	socket.on("trip:recreate", function(token, tripId){ //TODO Testing
		if(token)
		{
			var token = token.replace(/\"/g, "");
		}
		var id = tokensService.get(token);
		if(id)
		{
			Trip.findOne({'_id': tripId},tripService.onTripSave.bind(null, socket, id, true));
		}
	});

	socket.on("trip:cancel", function(token, tripId){
		tripService.TripCancel(tripId);
		console.log('TRIP CANCEL REQUESTED');
		if(token)
		{
			var token = token.replace(/\"/g, "");
		}
		var id = tokensService.get(token);
				console.log('user Id :',id);
		var letripId = tripId;
		if(id)
		{
					console.log('USER ID FOUND');
			var client = usersService.get(id);
			if(client)
			{
				console.log('CLIENT FOUND');
				Trip.findOne({'_id': tripId},function(err, letrip){
				if(letrip)
				{	
					letrip.Status = config.TRIP_STATUS_CANCELLED;
					letrip.save(function(err){
						if(!err)
						{	
							console.log('TRIP SAVED IN USER CANCEL');
						}
					});

					// var driverToken = tokensService.get(letrip.Driver.Session);
					var taxi = taxisService.get(letrip.Driver._id);
					if(taxi)
					{
						console.log('TAXI FOUND');
						if(letrip.InAdvance == true)
						{
							taxi.socket.emit("taxi:trip:inadvance:cancelled", letrip);
						}
						else
						{
							taxi.socket.emit("taxi:trip:cancelled", letripId);
						}
						socket.emit('trip:cancelled', letripId);

						if(taxi.currentTrip && taxi.currentTrip == letripId)
						{
							taxi.currentTrip = null;
						}
						delete taxi.trips[_.indexOf(taxi.trips, tripId)];
						taxi.tripCount--;

						if(taxi.tripCount <= 0)
						{
							taxi.isLocked = false;
						}
						
					}
					else
					{
						console.log('DIRECTLY SENDING TO CLIENT');
						socket.emit('trip:cancelled', letripId);
					}
				}


			});

				client.tripId = null;
				tripsManager.unregister(tripId);
			}
		}
	});



}

function setUpDevEvents(socket) {
	socket.on('dev:getalltaxipositions', function(token){
		console.log('Got your socket man ! ' + token);
		if(token == "supersecret")
		{
			var taxis = taxisService.getAllTaxis();
			var object = {};
			_.each(taxis, function(taxi){
				if(taxi._id != null && taxi.lat() != null && taxi.lng() != null && taxi.lastUpdatedTime != null)
				{
					object[taxi._id] = {lat: taxi.lat(), lng: taxi.lng(), lastUpdated: taxi.lastUpdatedTime};
				}
			});
			socket.emit('dev:alltaxipositions', object);
		}
		
	});
}

