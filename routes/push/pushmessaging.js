var _ = require('lodash');
var geolib = require('geolib');
var config = require('../config');
var gm = require('googlemaps');

var taxisService = require('../taxis/taxis');
var positionService = require('../positions/position');
var usersService = require('../users/users');

var Trip = require('../../data/models/Trips');
var Client = require('../../data/models/Client');
var Driver = require('../../data/models/Driver');

var gcm = require('node-gcm');


exports.SendMessageAndroid = function(Id, message)
{
	var sender = new gcm.Sender(config.GCM_API_KEY);
	var retryCount = 4;
	sender.send(message, Id, retryCount, function (err, result) {
    console.log(result);
});
}

exports.SendMessageIOS = function(Id, message)
{
	
}
