

exports.User = function(_id, socket) {
	
	return {
		_id: _id,
		socket: socket,
		clientObject: null,
		isLocked: false,
		tripId: null, 
		tripCount: 0,
		lastTripCreated: null
	}
};