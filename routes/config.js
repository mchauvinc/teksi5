// exports.DB_CONNECTION = 'mongodb://taxiherodev.cloudapp.net:27017/taxibooking'; //Node Alpha
// exports.DB_CONNECTION = 'mongodb://localhost:27017/taxibooking'; //Node Beta
//exports.DB_CONNECTION = 'mongodb://taxihero.cloudapp.net:27017/taxibooking'; //Node Production
exports.DB_CONNECTION = 'mongodb://localhost:27017/taxibooking'; //Node Production

exports.RADIUS = 5000; //In meters, Radius around the pickup position to look for drivers
var TIMEOUT = 15; //In seconds, Timeout for normal trips
var IN_ADVANCE_TIMEOUT = 15; //In seconds, Timeout for InAdvance trips
var	AVERAGE_SPEED = 15; //Taxi average speed in kilometers per hour

exports.TIMEOUT = TIMEOUT;
exports.IN_ADVANCE_TIMEOUT = IN_ADVANCE_TIMEOUT;
exports.MAX_TIMEOUT = 20;	//In seconds, max time the server will wait for when still getting answers from drivers
exports.LAST_CHANCE = 3;	//In seconds, time allowed to the last driver who answers to change his mind
exports.AVERAGE_SPEED = AVERAGE_SPEED;
exports.MAX_DRIVER_ANSWERS = 7;
exports.MAX_DISPATCH_DRIVERS = 25;
exports.MAX_RETRY_TRANSLATION = 50;

exports.TAXI_KILLER_TIMEOUT = 10; //In minutes
exports.USER_KILLER_TIMEOUT = 60 * 24; //In minutes 

exports.TRIP_STATUS_CREATED = 1;	//Don't change this
exports.TRIP_STATUS_CANCELLED = 0;	//Don't change this
exports.TRIP_STATUS_DONE = 2;		//Don't change this
exports.TRIP_REMINDER = 30; //In minutes, time before node sends a notification to driver for in advance trip

exports.TYPE_ANDROID = 1;	//Not used
exports.TYPE_IOS = 0;		//Not used

// Supported languaged by google maps api for translating addresses


exports.SUPPORTED_LANGUAGES = ["zh-CN", "zh", "en", "en-AU", "en-GB", "ar", "eu", "bg", "bn", "ca", "cs", "da", "de", "el",  
								"es", "eu", "fa", "fi", "fil", "fr", "gl", "gu", "hi", "hr", "hu", "id", "it", 
								"iw", "ja", "kn", "ko", "lt", "lv", "ml", "mr", "nl", "nn", "no", "or", "pl", "pt", 
								"pt-BR", "pt-PT", "rm", "ro", "ru", "sk", "sl", "sr", "sv", "tl", "ta", "te", "th", 
								"tr", "uk", "vi", "zh-TW"];


exports.LANGUAGE_CN = 'zh';
exports.LANGUAGE_EN = 'en';

exports.GCM_API_KEY = 'AIzaSyC8pMoS5Rv6dQb2-Ce4RoWG9j_g4Euasyk';	//Not used

// Put Here google maps api business credentials
exports.GM_API_ID = ''; //google-client-id
exports.GM_API_KEY = ''; //google-private-key


exports.price = function(distance){
	var price = 3;
	var d = distance - 1000;
		
	price = price + (Math.max(0,d)/115)*0.1;
	return price;
}

exports.duration = function(distance){//meters
	var duration = (distance * 60) / (AVERAGE_SPEED * 1000);
	return Math.floor(duration);
}

exports.latlng = function(lat, lng){
	return lat + ',' + lng;
}


exports.timestamp = function(isInAdvance){
	var timer = new Date();
	if(isInAdvance == true)
	{
		return timer.setSeconds(timer.getSeconds() + IN_ADVANCE_TIMEOUT);
	}
	else
	{
		return timer.setSeconds(timer.getSeconds() + TIMEOUT);
	}
}

exports.timeout = function(isInAdvance){
	if(isInAdvance == true)
	{
		return IN_ADVANCE_TIMEOUT * 1000;
	}
	else
	{
		return TIMEOUT * 1000;
	}
}

exports.formatLanguage = function(lang){
	if(lang.indexOf("_"))
	{
		var split = lang.split("_");
		return split[0];
	}
	else if(lang.indexOf("-"))
	{
		var split = lang.split("-");
		return split[0];
	}
	else
	{
		return lang;
	}
}


