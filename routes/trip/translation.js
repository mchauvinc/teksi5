var gm = require('googlemaps');
var util = require('util');
var _ = require('lodash');
var Q = require('q');
var http = require('http');
var config = require('../config');


if(config.GM_API_ID != '' && config.GM_API_KEY != '')
{
	gm.config('google-client-id', config.GM_API_ID);
	gm.config('google-private-key', config.GM_API_KEY);
}


var getAddress = function(latlng, lang)
{	
	var d = Q.defer();

		gm.reverseGeocode(latlng, function(err, data){
			console.log(data.status);
			if(data.status == 'OK')
			{
				 //d.resolve(data.results[0].formatted_address);
				var array = data.results[0].address_components;
				var street_number;
				var route;
				var sublocality;
				var locality;
				var country;
				var postal_code;

				_.each(array, function(x){
					if(street_number == undefined)
					{
						street_number = _.find(x.types, function(y){
							return y == 'street_number';
						});
						if(street_number != undefined)
						{
							street_number = x.long_name;
						}
					}


					if(route == undefined)
					{
						route = _.find(x.types, function(y){
							return y == 'route';
						});
						if(route != undefined)
						{
							route = x.long_name;
						}
					}

					if(sublocality == undefined)
					{
						sublocality = _.find(x.types, function(y){
							return y == 'sublocality';
						});
						if(sublocality != undefined)
						{
							sublocality = x.long_name;
						}
					}

					if(locality == undefined)
					{
						locality = _.find(x.types, function(y){
							return y == 'locality';
						});
						if(locality != undefined)
						{
							locality = x.long_name;
						}
					}

					// if(country == undefined)
					// {
					// 	country = _.find(x.types, function(y){
					// 		return y == 'country';
					// 	});
					// 	if(country != undefined)
					// 	{
					// 		country = x.long_name;
					// 	}
					// }

					// if(postal_code == undefined)
					// {
					// 	postal_code = _.find(x.types, function(y){
					// 		return y == 'postal_code';
					// 	});
					// 	if(postal_code != undefined)
					// 	{
					// 		postal_code = x.long_name;
					// 	}
					// }


				});

				var allresults = [];
				allresults.push(street_number);
				allresults.push(route);
				allresults.push(sublocality);
				allresults.push(postal_code);
				// allresults.push(locality);
				// allresults.push(country);

				var arrayFinal = _.filter(allresults, function(x){
					return x != undefined;
				});

	 			var string = '';

	 			_.each(arrayFinal, function(x){
	 				string = string.concat(x, ', ');
	 			});

	 			d.resolve(string.substr(0, string.length - 2));
			}
			else
			{
				d.reject("INVALID_STATUS_CODE");
			}
	}, false, lang);

	return d.promise;
};



var getPickupDestination = function(pickupA, destinationA, lang, translatedDescriptions, retryCount)
{
	var d = Q.defer();
	var i;

	if(retryCount)
	{
		i = retryCount;
	}
	else
	{
		i = 0;
	}
	if(i > config.MAX_RETRY_TRANSLATION)
	{
		translatedDescriptions['ERROR'] = "MAX_RETRY_TRANSLATION_REACHED";
		d.resolve();
	}
	else
	{
		var pickup = getAddress(pickupA, lang);
		var destination = getAddress(destinationA, lang);
		Q.allSettled([pickup, destination]).then(function(res){
			if(res[0].state == 'fulfilled' && res[1].state == 'fulfilled')
			{
				var description = {
							pickup: res[0].value,
							destination: res[1].value
						};
				translatedDescriptions[lang] = description;
				d.resolve(description);
			}
			else
			{
				
				console.log('ERROR: ', i);
				i++;
				setTimeout(function(){
					d.resolve(getPickupDestination(pickupA, destinationA, lang, translatedDescriptions, i));
				},1500);
			}
			
		});
	}

	return d.promise;
};


var getAllTranslations = function(languages, pickup, destination, callback)
{
	var promises = [];
	var translatedDescriptions = {};
	_.each(languages, function(x){
			var resolve = getPickupDestination(pickup, destination, x, translatedDescriptions);
			promises.push(resolve);	
	});

	
		Q.allSettled(promises).then(function(res){
				callback(translatedDescriptions);
			});

};


exports.getAllTranslations = getAllTranslations;


