var _ = require('lodash');
var geolib = require('geolib');
var gm = require('googlemaps');
var Q = require('q');
var io = require('socket.io');
var events = require('../event');
// var events = require('events');
// var events.emitter = new events.events.emitter();

var taxisService = require('../taxis/taxis');
var positionService = require('../positions/position');
var usersService = require('../users/users');
var tripsManager = require('../tripservice/trips');
// var usersService = users.users();

var Trip = require('../../data/models/Trips');
var Client = require('../../data/models/Client');
var Driver = require('../../data/models/Driver');
var pushService = require('../push/pushmessaging');
var config = require('../config');
var translation = require('./translation');

if(config.GM_API_ID != '' && config.GM_API_KEY != '')
{
	gm.config('google-client-id', config.GM_API_ID);
	gm.config('google-private-key', config.GM_API_KEY);
}


// variables used when trip is canceled
/*var temp_socket;
var temp_trip;
var temp_nearBy;
var cancelled = false;
var translatedDescriptions = {};
var descriptionSave = {pickup:'', destination:''};*/

exports.TripCancel = function(tripId)
{
	var tripM = tripsManager.get(tripId);
	if(tripM && tripM.temp_socket && tripM.temp_trip && tripM.temp_nearBy)
	{
		tripM.cancelled = true;
		// cancelled = true;
		console.log('Handling TIMEOUT for cancel');
		handleTimeout(tripM.temp_socket, tripM.temp_nearBy, tripM.temp_trip, null);	
	}

};
 
exports.onTripSave =  function(socket, userId, isARetry, err, trip){
	if(err)
	{
		console.log('Error occured in onTripSave: ', err);
	}
	if(trip && socket && userId)
	{

		var tripM = tripsManager.create(trip._id);
		tripsManager.register(tripM);

		console.log('TRIP ID: ', trip._id);
		console.log('IN onTripSave, userId: ', userId, ' isARetry: ', isARetry);
		var RADIUS = config.RADIUS;
		var TIMEOUT = config.TIMEOUT * 1000; //used for setTimeout function
		var IN_ADVANCE_TIMEOUT = config.IN_ADVANCE_TIMEOUT * 1000;
		var pickupPosition = positionService.Position(trip.Address.Pickup.Latitude, trip.Address.Pickup.Longitude);

		//Setting up the trip
		var tripDuration = config.duration(trip.distance());
		socket.emit('trip:create:success',  trip._id, tripDuration);
		
		// Getting all nearby taxis
		var allTaxis = taxisService.getAllTaxis();



				// Getting list of answers if this is a retry
		var driversList = trip.DriversProposed.proposed_drivers;

		// Getting list of negative answers
		var unwantedDrivers =  _.filter(driversList, function(x){
					return x.Response == 0;
				});

		// Getting only their corresponding id
		var unwantedTab = [];
		_.each(unwantedDrivers, function(x){
			unwantedTab.push(x.Driver._id);
		});

		console.log('BEFORE EACH NEARBY TAXIS');
		var nearByTaxis = _.filter(allTaxis, function(taxi, _id){
			
			var isUnwanted = _.find(unwantedTab, function(x){
				return x.toString() == taxi._id.toString();
			});
				console.log('IS UNWANTED: ', isUnwanted);
				console.log('NEARBY STATUS: ',  taxi.isWithinRadius(pickupPosition, RADIUS), taxi.isLockedForDispatch == false, (typeof isUnwanted == 'undefined'), taxi.currentTrip == null);
				return taxi.isWithinRadius(pickupPosition, RADIUS) && taxi.isLockedForDispatch == false && (typeof isUnwanted == 'undefined') && taxi.currentTrip == null;
				// return taxi.isWithinRadius(pickupPosition, RADIUS) && taxi.isLockedForDispatch == false && (typeof isUnwanted == 'undefined') && taxi.currentTrip == null;
				
				});

		console.log('AFTER EACH NEARBY TAXI');

		// If there are more than e.g. 5 nearby drivers, we dispatch the trip to only 5
		if(nearByTaxis.length > config.MAX_DISPATCH_DRIVERS)
		{
			nearByTaxis = nearByTaxis.slice(0, config.MAX_DISPATCH_DRIVERS);
		}

		var client = usersService.get(userId);
		console.log('NEARBY TAXI.LENGTH', nearByTaxis.length);
		// if(nearByTaxis.length == 0 || unwantedTab.length == nearByTaxis.length)
		if(nearByTaxis.length == 0)
		{
			console.log('TAXI UNAVAILABLE.....');
			socket.emit('trip:taxi:unavailable', trip._id);
			if(client)
			{
				// client.isLocked = false;
				client.tripId = null;
			}
			tripsManager.unregister(trip._id);
		} 
		else
		{

			var languages = [];
			
			_.each(nearByTaxis, function(taxi){
				taxi.isLocked = true;

				var match = _.find(languages, function(x){
					return taxi.language.toString() == x.toString();
				});
				if(typeof match == 'undefined')
					{
						languages.push(taxi.language.toString());
					}

			});


			translation.getAllTranslations(languages, trip.pickupLatLng(), trip.destinationLatLng(), function(res)
			{
				if(res['ERROR'] != undefined)
				{
					console.log('ERROR WHILE TRANSLATING: ', res['ERROR']);
					socket.emit('trip:taxi:unavailable', trip._id);
				}
				else
				{
					var tripM = tripsManager.get(trip._id);
					if(tripM)
					{
						console.log("Translations are done");

						var timer = new Date();
						var FIRST_CHANCE = timer.setSeconds(timer.getSeconds());
						var allTaxis = taxisService.getAllTaxis();
						
						tripM.descriptionSave.pickup = trip.Address.Pickup.Description;
						tripM.descriptionSave.destination = trip.Address.Destination.Description;
						tripM.translatedDescriptions = res;

						var driversList = trip.DriversProposed.proposed_drivers;
						var unwantedDrivers =  _.filter(driversList, function(x){
							return x.Response == 0;
						});
						var unwantedTab = [];
						_.each(unwantedDrivers, function(x){
							unwantedTab.push(x.Driver._id);
						});

						var nearByTaxis = _.filter(allTaxis, function(taxi, _id){
							
							var isUnwanted = _.find(unwantedTab, function(x){
								return x.toString() == taxi._id.toString();
							});

							return taxi.isWithinRadius(pickupPosition, RADIUS) 
									&& taxi.isLockedForDispatch == false 
									&& (typeof isUnwanted == 'undefined') 
									&& taxi.currentTrip == null;
						});

						if(nearByTaxis.length > config.MAX_DISPATCH_DRIVERS)
						{
							nearByTaxis = nearByTaxis.slice(0, config.MAX_DISPATCH_DRIVERS);
						}
										
						console.log('IS A RETRY STATUS: ', isARetry);

						_.each(nearByTaxis, function(taxi){

							var taxiId = taxi._id;
							var refused = _.find(unwantedTab, function(x){
								return x.toString() == taxiId.toString();
							});
							console.log('REFUSED STATUS: ', refused);
							if((isARetry == true && typeof refused == 'undefined') || isARetry == false)
							{
								trip.Address.Pickup.Description = tripM.translatedDescriptions[taxi.language].pickup; //tripM.descriptionSave.pickup + ', ' ...
								trip.Address.Destination.Description = tripM.descriptionSave.destination + ', ' +tripM.translatedDescriptions[taxi.language].destination;
								console.log('Sending trip to taxi: ', taxiId);
								
								taxi.isLockedForDispatch = true;
								taxi.socket.emit('trip:created', trip, trip.distance(), TIMEOUT);
							}

						});
						

						tripM.temp_socket = socket;
						tripM.temp_trip = trip;
						tripM.temp_nearBy = nearByTaxis;
						trip.Address.Pickup.Description = tripM.descriptionSave.pickup;
						trip.Address.Destination.Description = tripM.descriptionSave.destination;

						var taxisCount = tripM.temp_nearBy.length;

						//Function called when TIMER_STOP event is triggered
						var timerStop = function(letripId){
							console.log('GOT TIMER STOP tripId: ', letripId, ' trip._id: ', trip._id);
							 if(letripId == trip._id)
							 {
							 	console.log('GOING TO HANDLE TIMEOUT !');
								handleTimeout(socket, nearByTaxis, trip, userId);
							 }
						};
						
						// Event listener set once per onTripSave function calling
						var EVENT = 'TIMER_STOP:' + trip._id;
						events.emitter.once(EVENT, timerStop);

						// Trip was dispatched, event listeners are set so we can start to handle answers
						handleDriversAnswers(trip, taxisCount);
					}
					else
					{
						console.log('Error in translations');
					}					
				}


			});


		}
	}
	else
	{
		console.log('ERROR IN onTripSave, either trip or socket is null');
	}
};

// Function in charge of handling all driver answers for a trip
function handleDriversAnswers(trip, nearbyCount){
	console.log('IN HANDLE ANSWERS');
	var eventFired = false; //lock

	// Function called when one of the timers ended
	var stopTimer = function(tripId){
		console.log('GOT STOP TIMER1');
		if(eventFired == false && tripId == trip._id)
		{
			console.log('GOT STOP TIMER2');
			var EVENT = 'TIMER_STOP:' + trip._id;
			events.emitter.emit(EVENT, tripId);
			eventFired = true;
			events.emitter.removeListener('ANSWER_FROM_DRIVER', handleAnswer);
		}
	};

	/*Explanation about timers and this function's behavior:
	- Trip is dispatched to nearby drivers
	- This function is called an wait for answers
	- Every time a Driver sends an answer, timeout is reset
	- The previous step will repeat until every driver has answered, or maxTimeout is reached*/

	// Max countdown timer
	var maxTimeout = setTimeout(function(){
		stopTimer(trip._id);
	}, config.MAX_TIMEOUT*1000);

	var timeout = setTimeout(function(){
		stopTimer(trip._id);
	}, config.TIMEOUT*1000);

	var handleAnswer = function(tripId, count){
		if(tripId == trip._id)
		{
			console.log('GOT answer from driver, tripId: ', tripId, ' count: ', count);
			clearTimeout(timeout);
			if(count == nearbyCount || count >= config.MAX_DRIVER_ANSWERS)
			{
				setTimeout(function(){
					stopTimer(tripId);
				}, config.LAST_CHANCE*1000);
			}
			else
			{
				timeout = setTimeout(function(){
					stopTimer(tripId);
				}, config.TIMEOUT*1000);	
			}	
		}

		
	};
	events.emitter.on('ANSWER_FROM_DRIVER', handleAnswer);
};


function handleTimeout(socket, nearByTaxis, trip, userId){
	console.log('IN HANDLE TIMEOUT');
	var tripM = tripsManager.get(trip._id);
	console.log('TRIPM: ', tripM);
	if(tripM)
	{
		tripM.temp_socket = null;
		tripM.temp_trip = null;
		tripM.temp_nearBy = null;

		if(socket && nearByTaxis && trip)
		{
				// if trip was not cancelled during the countdown
			if(!tripM.cancelled)
			{	
				// Setting up pickupPosition
				var pickupPosition = positionService.Position(trip.Address.Pickup.Latitude, trip.Address.Pickup.Longitude);

				// Dispatching that the countdown ended
				_.each(nearByTaxis, function(taxi){
					console.log('IN EACH AFTER HANDLE TIMEOUT TAXI:', taxi._id);
				taxi.socket.emit('trip:countdown:ended', trip._id);
				taxi.isLockedForDispatch = false;
				});

				console.log('trip_id is : ', trip._id);

				// Getting the trip object from mongo db so we can get the list of drivers who accepted
				Trip.findOne({ '_id': trip._id }, function (err, res) {
				
				if(!err)
				{
					if(res){
						// Second lookup 
						if(res.Status == config.TRIP_STATUS_CANCELLED)
						{
							_.each(nearByTaxis, function(x){
								var n = taxisService.get(x._id);
								if(n && n.socket)
								{
									n.socket.emit('taxi:trip:cancelled',  trip._id);
								}
							});
						}
						else
						{
							// getting the drivers who proposed
							var drivers = res.DriversProposed.proposed_drivers;
							
							// Drivers who answered yes
							var filtered =  _.filter(drivers, function(x){
								return x.Response == 1;
							});

							// Drivers who answered
							var numberOfPropositions = res.DriversProposed.proposed_drivers.length;
							console.log('Total number of answers: ', numberOfPropositions, ' Number of YES answers: ', filtered.length);

							if(numberOfPropositions == 0 || filtered.length == 0)
							{
								// If no proposition, dispatching to client
								socket.emit('trip:taxi:noreply', trip._id);
								tripsManager.unregister(trip._id);
							}
							/*else if(filtered.length == 0)
							{
								socket.emit('trip:taxi:unavailable', trip._id);
							}*/
							else
							{	
								// Sorting drivers by nearest distance from pickup
								var sorted = filtered.sort(function(x){
									var taxi = taxisService.get(x.Driver._id);
									if(taxi)
									{	console.log('IN FILTERED', taxi.getDistance(pickupPosition));
										return taxi.getDistance(pickupPosition);
									}
								});
								
								// Selected becomes nearest taxi, sorted now contains all rejected ones
								var chosenTaxi = sorted.shift();
								console.log('Chosen Taxi is: ', chosenTaxi);
								var rejected = sorted;

								trip.Distance = trip.distance();
								trip.Duration = config.duration(trip.Distance);
								var lechosenTaxi = taxisService.get(chosenTaxi.Driver._id);

								if(lechosenTaxi)
								{
									var tripM = tripsManager.get(trip._id);
									if(tripM)
									{
										trip.Driver = lechosenTaxi.driverObject;
										trip.EstimatedArrivalTime = lechosenTaxi.getArrivalTime(trip.Address);

										trip.Address.Pickup.Description = tripM.translatedDescriptions[lechosenTaxi.language].pickup;
										trip.Address.Destination.Description = tripM.translatedDescriptions[lechosenTaxi.language].destination;
										
										trip.DriverLanguageAddress.PickupAddress = tripM.translatedDescriptions[lechosenTaxi.language].pickup;
										trip.DriverLanguageAddress.DropOffAddress = tripM.translatedDescriptions[lechosenTaxi.language].destination;

										lechosenTaxi.trips.push(trip._id);
										lechosenTaxi.tripCount++;


										// lechosenTaxi.isLocked = true;

										lechosenTaxi.socket.emit('taxi:trip:accepted',  trip);
										console.log('TAXI: ', lechosenTaxi._id, ' IS ACCEPTED.');

										trip.Address.Pickup.Description = tripM.descriptionSave.pickup;
										trip.Address.Destination.Description = tripM.descriptionSave.destination;		
									}
									else
									{
										console.log('ERROR IN SENDING TRIP, TRIPM UNKNOWN');
									}

									
								}
								else
								{
									console.log('ERROR: Could not get taxi');
								}
								trip.save(function(err){
									if(!err)
									{}
								});

								// Sending trip accepted to the client
								socket.emit('trip:taxi:driver:accepted', trip);

								// Here something to handle driver and client if trip is in advance
								if(trip.InAdvance == true)
								{
									console.log('THIS TRIP IS IN ADVANCE');

									lechosenTaxi.isLocked = true;
									
									var reminder = config.TRIP_REMINDER; //Amount of minutes before we remind the driver/user of the trip
									var timerNow = new Date();	//Date now
									var timerRequest =  trip.RequestTime;	//Request time of the trip
									var reminderTimer = timerRequest.setMinutes(timerRequest.getMinutes() - reminder); //Time of reminder
									var remindTimeout = reminderTimer - timerNow; //Timeout before reminding
									
									console.log('Push notification set in: ', remindTimeout, ' Milliseconds');
									
									setTimeout(function(){
										console.log('TRIP IN ADVANCE REMINDER TIMEOUT ENDED. Sending to taxi ', lechosenTaxi._id);
										// Dispatching to taxi
										var ataxi = taxisService.get(lechosenTaxi._id);
										if(ataxi && ataxi.socket)
										{
											trip.Address.Pickup.Description = translatedDescriptions[ataxi.language].pickup;
											trip.Address.Destination.Description = translatedDescriptions[ataxi.language].destination;

											ataxi.socket.emit('trip:starting:soon', trip);

											trip.Address.Pickup.Description = descriptionSave.pickup;
											trip.Address.Destination.Description = descriptionSave.destination;
										}
										// Dispatching to client

										var leclient = usersService.get(trip.Client._id);
										if(leclient && leclient.socket)
										{	
											// Client socket could have changed since the begining of remindTimeout 
											leclient.socket.emit('trip:starting:soon', trip);
										}
									}, remindTimeout);

								}
								else
								{	
									var ataxi = taxisService.get(lechosenTaxi._id);
									console.log('THIS IS A NORMAL TRIP');
									lechosenTaxi.currentTrip = trip._id;
									var client = usersService.get(userId);
									if(client)
									{	
										client.tripId = trip._id;
										// client.isLocked = true;
									}
									var taxiPosition = ataxi.position.toDict();
									var position = {
												_id: lechosenTaxi._id,
												TripId: trip._id,
												Position: {lat: taxiPosition.latitude, lng: taxiPosition.longitude},
												RemainingTime: ataxi.getArrivalCountdown(trip.Address),
												ArrivalTime: ataxi.getArrivalTime(trip.Address)
											};
									socket.emit('yourdriver:position', position);
								}

								var wantedTab = [];
								_.each(filtered, function(x){
									wantedTab.push(x.Driver._id);
								});
								console.log('WANTED TAB', wantedTab);
								_.each(wantedTab, function(x){
									var n = taxisService.get(x);

									if(n._id.toString() != lechosenTaxi._id.toString())
									{	
										console.log('Taxi: ', n._id, ' is refused.');
										n.socket.emit('taxi:trip:refused',  trip._id);
										// Unlocking these taxis
										n.isLocked = false;
									}
								});	
							}		
						}
					}
					else
					{
						console.log('UNABLE TO FIND THE TRIP')
					}
				}
				
				});
			}
			else
			{	//If trip was cancelled
				_.each(nearByTaxis, function(taxi){ 
					// dispatching to every nearBy taxi
				taxi.socket.emit('taxi:trip:cancelled', trip._id);
				taxi.isLocked = false;
				taxi.isLockedForDispatch = false;
				});
				tripM.cancelled = false;
				tripsManager.unregister(trip._id);
			}
		}
		else
		{
			console.log('ERROR IN handleTimeout');
		}

	}

}


