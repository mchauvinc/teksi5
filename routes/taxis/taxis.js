var taxi = require('./taxi');
var tokensService = require('../tokens/tokens');
// var tokensService = tokens.tokens();
var _ = require('lodash');
var config = require('../config');

var taxis = {};

exports.register = function(taxi){
			taxis[taxi._id] = taxi;
		};

exports.create = function(_id, socket){
			return taxi.Taxi(_id, socket);
		};


exports.get = function(_id){
			return taxis[_id];
		};
exports.unregister = function(_id){
			delete taxis[_id];
		};

exports.getAllTaxis = function(){
			return taxis;
		};

// taxi killer will automatically unregister taxis
exports.taxiKiller = function(){
			var Taxis = this.getAllTaxis();
			var Timestamp = Date.now();
			var allTokens = tokensService.getAllTokens();
			// Looking for every taxi
			_.each(Taxis, function(x){
				var currentTaxi = x;
				// If current taxi already updated his position
				if(currentTaxi.lastUpdatedTime)
				{	var diff = 0;
					
					diff = Timestamp - currentTaxi.lastUpdatedTime;
					var TIMEOUT = config.TAXI_KILLER_TIMEOUT * 60 * 1000;
					var criteria = (diff > TIMEOUT);
					if(criteria == true)
					{
						if(currentTaxi.tripCount <= 0 && currentTaxi.isLocked == false && currentTaxi.currentTrip == null);
						{	console.log('Still true');
							var letoken =  _.findKey(allTokens, function(x) {
	  						return x == currentTaxi._id.toString();
							});
							console.log('Registered taxis are: ', letoken);
						
							if(letoken)
							{	
								console.log('TAXI', currentTaxi._id, 'WAS UNREGISTERED');
								tokensService.unset(letoken);
								delete taxis[currentTaxi._id];
							}
						}
					}
				}
			});
		};


