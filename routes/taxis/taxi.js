var position = require('../positions/position');
var geolib = require('geolib');
var moment = require('moment');
var config = require('../config');


exports.Taxi = function(_id, socket) {

	var AVERAGE_SPEED = config.AVERAGE_SPEED;
	return {
		_id: _id,
		position: position.Position(0,0),
		lastUpdatedTime: null,
		driverObject: null, //Driver object in mongodb database
		socket: socket, //Socket of the driver
		isLocked:false,
		tripCount: 0,
		token: null,
		currentTrip: null,
		arrivedAtPickup: false,
		trips: [],
		language: '',
		lastTripCompleted: null,
		isLockedForDispatch: false,
		
		getDistance: function(center) {
			var center = center.toDict();// {latitude: center.lat, longitude: center.lng};
			var position = this.position.toDict();
			console.log('Position: ', position.latitude, position.longitude);

			return geolib.getDistance({latitude: center.latitude , longitude: center.longitude}, {latitude: position.latitude , longitude: position.longitude});
		},
		//TODO CHANGE WITH IN ADVANCE BOOKINGS
		getArrivalCountdown: function(Address) { 
			var center = {latitude: Address.Pickup.Latitude, longitude: Address.Pickup.Longitude};
			var position = this.position.toDict();
			var distanceFromPickup = geolib.getDistance(center, {latitude: position.latitude , longitude: position.longitude}); //Meters
			var remainingTime = distanceFromPickup/((AVERAGE_SPEED*1000)/3600); //seconds
			return  Math.round(remainingTime);
		},
		//TODO
		getArrivalTime: function(Address) { //gives arrival time in UTC format TODO CHECK WITH IN ADVANCE BOOKINGS
			var center = {latitude: Address.Pickup.Latitude, longitude: Address.Pickup.Longitude};
			var position = this.position.toDict();	
			var position2 = {latitude: position.latitude , longitude: position.longitude};
			var distanceFromPickup = geolib.getDistance(center, position2); //Meters
			var remainingTime = distanceFromPickup/((AVERAGE_SPEED*1000)/3600); //seconds
			var time = new Date();
			time.setSeconds(time.getSeconds() + remainingTime);
			return  time;
		},
		isWithinRadius: function(center, radius){
			return geolib.isPointInCircle(this.position.toDict(), center.toDict(), radius);
		},
		// Utility functions, proxy for position object
		lat: function(v){
			this.lastUpdatedTime = Date.now();
			return this.position.lat(v);
		},
		lng: function(v){
			return this.position.lng(v);
		},
		updatePosition: function(lat, lng){
			this.lat = lat;
			this.lng = lng;
			this.lastUpdatedTime = Date.now();

		}
	}
};