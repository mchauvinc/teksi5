exports.Position = function(lat, lng) {
	var x = lat, y = lng;
	return {
		lat: function(value){
			if(value){
				x = value;
			}
			return x;
		},
		lng: function(value){
			if(value){
				y = value;
			}
			return y;
		},
		toDict: function()
		{
			return {
				latitude: this.lat(),
				longitude: this.lng()
			};
		}
	}
}